//Applikationsvariablen
const express = require("express");
const app = express();
const PORT = process.env.PORT || 3000;

//Cross-origin fuer alle abfragen
var cors = require("cors");
app.use(cors());

//Static files
app.use(express.static("./public"));

//JSON files
var tasks = require("./tasks.json");

//Variablen
var taskID = 0;
var task = [];

//Bodyparser
const bodyParser = require("body-parser");
app.use(bodyParser.json());

const fs = require("fs");

//Applikation auf Port starten
app.listen(PORT, () => {
 console.log("Server running on port 3000");
});

//Get all tasks
app.get("/tasks", (req, res, next) => {
    updateLastTaskID();
    res.send(task);
    //res.send(JSON.stringify(tasks));
});

//Get all taskIDs
app.get("/taskID", (req, res, next) => {
    let id = [];
    id.push(updateLastTaskID());
    res.send(id);
});

//Get a task with specific status (done/open)
app.get("/tasks/:id", (req,res,next) => {
    var requestedID = req.params.id;
    var rawData = fs.readFileSync("tasks.json");
    let tasksJSON = JSON.parse(rawData);

    var sendData = [];

    for(let i = 0; i < tasksJSON.length; i++){
       if(tasksJSON[i].id == requestedID){
            sendData.push(tasksJSON[i]);
        }
        //taskID = tasksJSON[i].id;
    }

    if(sendData.length == 0){
        res.status('401').send('<script>location.href = "error.html";</script>');
    }
    else {
        res.send(sendData);
    }
});

//Add a new task
app.post('/tasks', function(req, res) {
    var daten = req.body;

    updateLastTaskID();
    let newTask = new nTask(taskID + 1, false, daten.title);
    //task.push(newTask);

    //write new task in tasks.json
    fs.readFile('tasks.json', (err, file) => {
        let tArray = JSON.parse(file);
        tArray.push(newTask);
        fs.writeFile('tasks.json', JSON.stringify(tArray), function(err, result) {
            if(err) console.log('error', err);});
    });
    res.send('Task added' + JSON.stringify(task));
});

function nTask(index, done, title) {
    this.id = index;
    this.done = done;
    this.title = title;
};

//Update a specific task
app.put('/update', (req, res) => {
    var udaten = req.body;
    var uid = udaten.id;
    var udone = udaten.done;
    var utitle = udaten.title;

    var updatedTask = new nTask(uid, !udone, utitle);
    
    //write new task in tasks.json
    fs.readFile('tasks.json', (err, file) => {
        let taskArray = JSON.parse(file);
        for(let t of taskArray) {
            if (t.id === uid) {
                t.done = !t.done;
            }
        }
        fs.writeFileSync('tasks.json', JSON.stringify(taskArray), function(err, result) {
            if(err) console.log('error', err);});
        });
        res.send('Task updated: ' + JSON.stringify(updatedTask));
});

//delete 'done' tasks
app.delete('/delete/:status', function (req,res) {
    let status = req.params.status;
    console.log(status);
    fs.readFile('tasks.json',(err,file) => {
        let taskArray = JSON.parse(file);
        let newArray =  [];
        for(let t of taskArray) {
            if (t.done == !status) {
                //console.log("task done");
              newArray.push(t);
            }
        }
        //console.log(newArray);
        fs.writeFileSync('tasks.json', JSON.stringify(newArray), function(err, result) {
            if(err) console.log('error', err);});
    });
    res.send('Tasks deleted');
});


//Error Handling
app.get('*', function(req, res){
    if (req.accepts('html')) {
        res.status('404').send('<script>location.href = "error.html";</script>');
       //res.send('404', '<script>location.href = "error.html";</script>');
       return;
    }
    if (req.accepts('html')) {
        res.send('500', '<script>location.href = "error.html";</script>');
        return;
     }
  });

function updateLastTaskID(){
    var rawData = fs.readFileSync("tasks.json");
    task = JSON.parse(rawData);
    taskID = 0;
    for (let t of task) {
        taskID = t.id;
    }
}