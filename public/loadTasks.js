let offlineNotificationIndex = null;
let preurl = "";

function showOfflineInfo() {
    offlineNotificationIndex.innerHTML = 'You are currently offline.';
    offlineNotificationIndex.className = 'showOfflineNotification done';
}
function hideOfflineInfo() {
    offlineNotificationIndex.className = 'hideOfflineNotification';

    getTasks('http://localhost:3000/tasks', false);

}

window.addEventListener('online', hideOfflineInfo);
window.addEventListener('offline', showOfflineInfo);

//List all "open" tasks
function open()
{
    //preurl = 'http://localhost:3000/';
    offlineNotificationIndex = document.getElementById('info');
    if( navigator.onLine) {
        getTasks('https://notestodo.herokuapp.com/tasks', false);
    }
    else{
        console.log("offline");
        showOfflineInfo();
    }
}

//List all "done" tasks
function done() {
    getTasks('https://notestodo.herokuapp.com/tasks', true);
    document.getElementById('delete').addEventListener('click', function (event) {
        event.preventDefault();
        deleteTasks();
    });
}

//get tasks with given status (open/done)
function getTasks(url,stats){
    fetch(url, {method: 'GET'}).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            return;
        }
        response.json().then(function (tasks) {
            taskID = 0;

            //console.log(tasks);
            for (var i=0; i < tasks.length; i++) {
                taskID = tasks[i].id;
                if (tasks[i].done == stats) {
                    listTask(tasks[i]);
                }
            }
            //console.log(taskID);
        })
    })
}

function deleteTasks() {
    let url = 'https://notestodo.herokuapp.com/delete/true';
    fetch(url, {method: 'DELETE'}).then(response => {
        console.log(response);
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
        }
        else{
            console.log('Tasks deleted ' + response.status);
        }
    });
}


function getLastTaskId(url){
    fetch(url, {method: 'GET'}).then(response => {
        //console.log(response)
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            return;
        }

        response.json().then(function (id) {
        });
    })
}    

//Add a new task to the list
function add(event) {
    event.preventDefault();

    let url = 'https://notestodo.herokuapp.com/tasks';
    var title = document.getElementById('InputTitle').value;
    var id = getLastTaskId('https://notestodo.herokuapp.com/taskID') + 1;
    var newTask = {id: id, done: false, title: title};

    fetch( url, {
        method: 'POST',
        body: JSON.stringify(newTask),
        headers: {'Content-Type': 'application/json'}
    }).then( response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
        }
        else {
            console.log('Task erfolgreich hinzugefügt');
            listTask(newTask);
            document.getElementById('InputTitle').value = "";
        }
    });
}

function syncTask() {
    let db;
    var DBRrequest = self.indexedDB.open("keyval", 1);
    DBRrequest.onsuccess = function (event) {
        db = event.target.result;


    }
}

function updateTask(task, checkbox) {
    let url = 'https://notestodo.herokuapp.com/update';
    fetch( url, {
        method: 'PUT',
        body: JSON.stringify(task),
        headers: {'Content-Type': 'application/json'}
    }).then( response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
        }
        else {
            console.log('Task erfolgreich geändert');
            checkbox.checked = true;
            checkbox.disabled = true;
        }
    });
}

//Load a Task into the DOM
function listTask(task){
    let newGroup = document.createElement('div');
    newGroup.className= 'input-group-prepend todo';

    let newRow = document.createElement('div');
    newRow.className= 'input-group-text';

    let newCheckbox = document.createElement('input');
    newCheckbox.type='checkbox';
    if(task.done == true)
    {
        newCheckbox.checked = true;
    }
    newCheckbox.addEventListener('click', function (event) {
        event.preventDefault();
        updateTask(task, newCheckbox);
    });

    let newText = document.createElement('span');
    newText.className = 'input-group-text';
    newText.innerHTML = task.title;

    newRow.appendChild(newCheckbox);
    newRow.appendChild(newText);
    newGroup.appendChild(newRow);

    document.getElementById('taskContainer').appendChild(newGroup);
}


