importScripts('./idb-keyval.js');

var cacheName = 'notes';
var offlineUrl = './index.html';

// Cache our known resources during install
self.addEventListener('install', event => {
	event.waitUntil(
		caches.open(cacheName)
		.then(cache => cache.addAll([
			'./loadTasks.js',
			'./idb-keyval.js',
			'./notesstyles.css',
			'./error.html',
			'./index.html',
			'./img/android-chrome-512x512.png',
			'/img/android-chrome-192x192.png',
			'/img/apple-touch-icon.png',
			'/img/favicon.ico',
			'/img/favicon-32x32.png',
			'/img/favicon-16x16.png',
			'/img/icon.png',
			'/manifest.json',
		]))
	);
});

// Cache any new resources as they are fetched
self.addEventListener('fetch', event => {
	if (event.request.method === 'GET' && event.request.headers.get('accept').includes('text/html')) {
		event.respondWith(
		fetch(event.request.url).catch(error => {
			return caches.match(offlineUrl);
			})
		)
	}
	else {
		event.respondWith(
			caches.match(event.request, {ignoreSearch: true})
				.then(function (response) {
					if (response) {
						return response;
					}
					var requestToCache = event.request.clone();
					return fetch(requestToCache).then(
						function (response) {
							if (!response || response.status !== 200) {
								return response;
							}
							var responseToCache = response.clone();caches.open(cacheName)
							// .then(function(cache) {
							// 	// cache.put(requestToCache, responseToCache);
							// });
							return response;
						});
				})
		);
	}
});

self.addEventListener('sync', (event) => {
	if (event.tag === 'addTask') {
		event.waitUntil(
			idbKeyval.get('sendMessage').then(value =>
			fetch('https://notestodo.herokuapp.com/tasks', {
				method: 'POST',
				headers: new Headers({ 'content-type': 'application/json' }),
				body: JSON.stringify(value)
			})));
		idbKeyval.del('sendMessage');
	}
});
